/// An example to show that algorithms can become more complex
/// and more difficult to follow and to understand what
/// the algorithm is actually doing
/// Also known as the Egyptian Multiplication
// n and m:  N (naturual numbers)
fn riddle_original(n: u64, m: u64) -> u64 {
    let mut x = n;
    let mut y = m;
    let mut p = 0;
    while x >= 1 {
        if x % 2 == 0 {
            x = x / 2;
        } else {
            p = p + y;
            x = (x - 1) / 2;
        }
        y = 2 * y
    }
    p
}

fn is_divider_original(x: &u64, z: &u64) -> bool {
    let mut y: i64 = *z as i64; // i64 required, substraction may lead to neg numbers
    while y > 0 {
        y = y - *x as i64;
    }
    if y == 0 { true } else { false }
}

fn main() {
    dbg!(riddle_original(345, 753));
    dbg!(riddle_original(33, 777));
}