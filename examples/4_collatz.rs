/// n: a natural number 
/// Lothar Collatz asked the question:
/// Is there any n that would cause this algorithm to run indefinitely
/// Until today no n was found that would do that, 
/// but there's no evidence that there's not one n that would do that either
/// The result is "always" 0, but following the iterations
/// one can see that m jumps up and down. Therefore the question isn't
/// that easy to answer
fn collatz(n: u64) -> f64 {
    let mut m = n as f64;
    dbg!(&m);
    while m > 1.0 {
        if m % 2.0 == 0.0 {
            m = m / 2.0;
            dbg!(&m);
        } else {
            m = 3.0 * m + 1.0;
            dbg!(&m);
        }
    }
    m
}
fn main() {
    dbg!(collatz(25));
}