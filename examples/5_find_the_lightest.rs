/// find the lightest of [n] men


/// less runtime-efficient. iterations are dependent on the position of
/// the lightest man in the list / array. (fix check book)
/// the runtime grows n², ie. the runtime having 2 men in the list
/// is 2 iteratons, 3 men = grows +6 iterations, 4 men = grows +12 iterations and so forth
/// uses an exhaustive algorithm, also known as brute-force.
/// checks all men against each other 
fn method_1_original(men: &[u8]) -> usize {
    let mut lightest = 9999;
    for i in 0..men.len() {
        
        if is_lightest_original(i, &men) { lightest = i; }
    }
    lightest
}
fn is_lightest_original(i: usize, men: &[u8]) -> bool {
    let mut lightest = true;
    for j in 0..men.len() {
        if j != i {
            println!("{:?}", i);
            if men[i] > men[j] {
                lightest = false;
            }
        }
    }
    lightest
}

/// more runtime-efficient. requires 4 iterations of the loop
/// the heaviest is out. two men are compared, the heavier is out
/// the lighter one is compared against the next
fn method_2_original(men: &[u8]) -> usize {
    let mut lightest = 0; // the lightest known man is the first in the list
    for i in 1..men.len() {
        if men[i] < men[lightest] { lightest = i; }
    }
    lightest
}
/// rust functional variation
fn find_the_lightest_variation(men: &[u8]) -> usize {
    men.iter()
        .enumerate()
        .fold(0, | m, (lightest, current) | if men[m] > *current { lightest } else { m } )
}

fn main() {
    let man_1 = 73;
    let man_2 = 102;
    let man_3 = 88;
    let man_4 = 69;
    let man_5 = 74;
    let men = vec![man_1, man_2, man_3, man_4, man_5];
    dbg!(method_1_original(&men));
    dbg!(method_2_original(&men));
    dbg!(find_the_lightest_variation(&men));
}