/// Sum up all natural numbers from 1 to 100
/// Problem: Summation
/// Input: A natural number
/// Calculation: The sum of all s of all natural numbers that are <= n

/// Examples from the books can be identified by the fn suffix _original
/// Variations are improved versions of the original
/// None of the variations support s > n 

// imperative (original from book)
// mutates and returns s
// error prone: correct result only for s = 0
fn add_original(mut s: u64, n: u64) -> u64 {
    for i in s..=n {
        s += i;
    }
    s
}

// rm mut s, since start is always 0 according to calculation objective
fn add_variation_1(n: u64) -> u64 {
    let mut t = 0;
    for i in 0..=n {
        t += i;
    }
    t
}

// mathematically more efficient gaussian summation (original from book)
// requires only 3 operations instead of 0..=n operations
fn gauss_original(n: u64) -> u64 { n * ( n + 1) / 2 }

// gaussian summation variation
// supports s != 0
// only 7 operations
fn gauss_variation(s: u64, n: u64) -> u64 {
    (n * n + n) / 2 - (s * s - s) / 2
}

// rust idiomatic functional by hand
fn fold(s: u64, n: u64) -> u64 {
    (s..=n).into_iter().fold(0, |acc, x| acc + x)
}

// rust idiomatic functional provided
// least error prone
fn sum(s: u64, n: u64) -> u64 { (s..=n).sum() }

fn main() {
    dbg!(add_original(0, 100));
    dbg!(add_variation_1(100));
    dbg!(gauss_original(100));
    dbg!(gauss_variation(0, 100));
    dbg!(fold(0, 100));
    dbg!(sum(0, 100));
}