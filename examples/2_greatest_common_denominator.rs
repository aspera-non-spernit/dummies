/// Find the greatest common denominator
/// Problem: Greatest Common Demoninator
/// Input: Two natural numbers n and m
/// Calculation: The greatest natural number t, that is a divider of both n and m 

/// Insight of performance are the result of an naive analyisis of the solutions


// Runs "indefinitely" assuming std::u64::MAX represents N of all natural numbers
// At least it may run very long.
// Therefore not efficient and suitable for computing
fn gcd1_original(n: u64, m: u64) -> u64 {
    let mut t: u64 = 1;
    for x in 0..std::u64::MAX {
        if n % x == 0 && m % x == 0 {
            t = x;
        }
    }
    t
}
// Much more efficient solution by limiting search space
// to 1..to the smaller number of n,m since
// if n > m, n != divider of m and
// if m > n, m != divider of n
// starts at x == 2
fn gcd2_original(n: u64, m: u64) -> u64 {
    let mut t: u64 = 1;
    for x in 2..=std::cmp::min(n, m) {
        if n % x == 0 && m % x == 0 {
            t = x;
        }   
    }
    t
}

fn gcd3_original(n: u64, m: u64) -> u64 {
    let mut t: u64 = 1;
    for x in 2..=std::cmp::min(n, m) {
        if is_divider_original(&x, &m) && is_divider_original(&x, &n) {
            t = x;
        }   
    }
    t
}

// less runtime-efficient algorithm
fn is_divider_original(x: &u64, z: &u64) -> bool {
    let mut y: i64 = *z as i64; // i64 required, substraction may lead to neg numbers
    while y > 0 {
        y = y - *x as i64;
    }
    if y == 0 { true } else { false }
}
// rust idiomatic functional by hand
fn gcd_variation_1(n: u64, m: u64) -> u64 {
    (1..=std::cmp::min(n, m)).filter(|x| n % x == 0 && m % x == 0).last().unwrap()
}

// rust idomatic functional lib provided
/**

extern crate num;
use num::Integer;
fn gcd_variation_2(m: u64, n: u64) -> u64 { n.gcd(m) }

**/

// All variations run, but only two of them are runtime-efficient
// and suitable for computing
fn main() {
    // dbg!(gcd1_original(42, 759));        // runs unnecessarily too long
    dbg!(gcd2_original(42, 759));
    dbg!(gcd3_original(42, 759));
    dbg!(gcd_variation_1(42, 759));
    // dbg!(gcd_variation_2(42, 759));      // requires extern crate num
}