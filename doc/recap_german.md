Eine Kurzzusammenfassung des Buches "Algorithmen und Datenstrukturen für Dummies", die ich während
des Lesens zusammen mit den rust-Beispielen angefertigt habe.

***Kein Gewähr auf Vollständigkeit und Richtigkeit.***


# Kapitel 1 Algorithmen

### Der Algorithmus

- Algorithmen werden häufig in Pseudocode geschrieben
- unabhängig von Programmiersprachen, allgemeingültig
- Mathematische Modellierung wird für die Formulierung von Algorithmen angewendet
- nur machbare Dinge werden formuliert
- ist eine genaue Anleitung eines Ablaufs

**Algorithmus:**

> „Ein Algorithmus ist ein wohldefiniertes, endlich beschreibbares, schrittweises Verfahren zum Lösen eines Problems oder der Erfüllung einer Aufgabe“ (S. 26)

Aufgabe: Summationsproblem
Problem: Summation
Eingabe: eine natürliche Zahl
Berechne: die Summe s aller natürlichen Zahlen, die kleiner oder gleich n sind

Siehe: 1_summation.rs

- Es wird zwischen *Berechnungs-, Entscheidungs- und Optimierungsproblemen* unterschieden. Entscheidungsprobleme können mit Ja oder Nein beantwortet werden. Ein Optimierungsproblem sucht die beste Lösung auf einer Menge an möglichen Lösungen.
- Eine Lösung die ein Algorithmus zu einem Problem liefert muss natürlich korrekt sein.
- Eine Probleminstanz ist ein Problem für ganz bestimmte Eingabedaten (z.B. Summe 1..100, 20..30)

### Die Theorie der Berechenbarkeit und die RAM

- Etwas ist rechenbar, wenn es von einer wohldefinierten, einfachen Maschine ausgeführt werden kann.
- Das bekanntese Maschinenmodell ist die 'Touringmaschine*.
- Maschinen die, genauso viel wie die Touringmaschine rechnen können, aber nach verschiedenen Prinzipien funktionieren, sind *Touring-vollständig*
- Für die Theorie der Algorithmen wird die *RAM* verwendet (siehe Glossar). Eine RAM ist Touring vollständig und heutigen Computern sehr ähnlich

Die RAM hat folgende Eigenschaften:

- sie hat Speicherstellen für beliebige endliche Daten
- diese können mit Zuweisungen und den üblichen arithmetischen Operationen manipuliert werden
Daneben kann eine RAM auch Kontrollanweisungen ausführen, nämlich:
- bedingte Anweisungen
- Schleifen, sowie
- (rekursive) Funktions- und Prozedurdefinitionen

Gut zu wissen: Reele Zahlen sind mit endlichem Speicher nicht exakt darstellbar und gehöhrn nicht zu den elementaren Daten der RAM. 

### Die Bewertbarkeit von Algorithmen

- Die Effizienz von Algorithmen ist bewertbar.
- Die *Laufzeit-Effizienz* ist das wichtigste Effizienzkriterium

Beispiel:

    1. s = 2 *  (2 + 1)  / 2 = 3
    2. s = 1 + 2

**Laufzeit-Effizienz:**

> „Die Laufzeit-Effizienz eines Algorithmus sagt etwas darüber aus, wie die Zahl der Rechenschritte, die der Algorithmus für eine bestimmte Eingabe ausführt von deren Größe abhängt“

### Pseudocode

- Die Sprache des Pseudocodes muss nicht bis ins Detail definiert sein
- aber muss immer klar und deutlich machen wie die einzelnen Schritte auf der RAM in zählbare Aktionen umgesetzt werden kann
- es können mathematische Ausdrücke verwendet werden

### Algorithmen sind keine Probleme

Übersetzt: KISS . Die Lösung steht im Vordergrund, nicht das Problem.

Aufgabe: Größter gemeinsamer Teiler
Problem: Größter gemeinsamer Teiler
Eingabe: zwei natürliche Zahlen n und m
Berechne: die größte natürliche Zahl t, die sowohl ein Teiler von n als auch von m ist.

Siehe: 2__greatest_common_denominator.rs

### Algorithmen haben zählbare Schritte
      
- jeder einzelne Schritt kann nachvollzogen werden.

### Algorithmen sollten korrekt sein

- Sie sollten bei jeder zulässigen Eingabe, ein korrektes Ergebnis zurückgeben.
- Es gibt kein Patentrezept, Fehler zu vermeiden.
- Sie sind oft schwer zu verstehen, Fehler sind schwerer zu vermeiden

Die vorherigen Beispiele beinhalten Fehler. So kann es sein, dass ein Algorithmus bei *n = 0* und *m = 100* funktioniert, bei *n = 100* und *m = 0* jedoch ein falsches Ergebnis errechnet. 

Siehe: 1_summation.rs und 2_greatest_common_denominator.rs für inkorrekte Algorithmen und, 3_riddle.rs für einen komplexeren, schwer zu verstehenden Algorithmus.

### Algorithmen können sich aufhängen

- Es kann zu einer unendlichen Laufzeit kommen
- Unendlich laufende Algorithmen können nicht auf Korrektheit überprüft werden
- Eine Ausdruck wie *„x ← max(x der Menge N) | x teilt m und x teilt n“* ist nicht erlaubt
- Das Problem kann besonders bei while-Schleifen und rekursiven Funktionen auftreten
      
Siehe: gcd_1_original in 2_greatest_common_denominator.rs, siehe 4_collatz.rs

### Das Halteproblem ist unlösbar

Aufgabe: das Halteproblem
Problem: das Halteproblem
Eingabe:  ein Algorithmus A und eine Eingabe x
Berechne: entscheide, ob A gestarte auf x irgendwann anhält oder sich der Algorithmus aufhängt

### Korrektheit beweisen

- bei einer unendlich großen Menge möglicher Eingaben, kann die *Korrektheit* nicht bewiesen werden.
- Ein Weg die Korrektheit eines Algorithmus zu beweisen ist der *Induktionsbeweis*.
- Dazu muss klar sein, was korrekt im Speziellen bedeutet. Dazu wird die *Schleifeninvariante* genutzt. Eine Größe, die vor als auch nach einem Schleifendurchlauf konstant bleibt, oder im Allgemeinen, gültig bleibt.
- Das finden einer geeigneten Schleifeninvariante ist nicht immer einfach.

**Induktionsbeweis:** 

> „Wenn bis zum i-ten Schleifendurchlauf alles korrekt funktioniert hat, dann funktioniert auch im nachfolgenden *i + 1*-tem Schleifendurchlauf alles korrekt.“

Die Schleifeninvariante (SI) am Beispiel des riddle Algorithmus:

*n * m = x, y + p*


**Korrektheitsbeweis:**

Der *Korrektheitsbeweis* läuft dann so:

Siehe Buch Seite 45

> Korrektheitsbeweise von Algorithmen sind häufig Induktionsbeweise, die über die Durchläufe einer Schleife hinweg die Gültigkeit einer Schleifeninvariante belegen. Dabei ist eine Schleifeninvariante etwas, das über die Schleifendurchläufe hinweg >>invariant<< bleibt.

Das finden einer geeigneten Schleifeninvariante ist nicht immer einfach.


# Kapitel 2 Qualität von Algorithmen

- Ein Algorithmus muss nicht nur im Prinzip seine Aufgabe erfüllen, sondern darf auch nur eine vernünftige Menge an Ressourcen wie Zeit oder Speicher verbrauchen.
- Der Ressourcenverbrauch von Algorithmen ist also ein Qualitätsmerkmal.
- Dieser Verbrauch muss bestimmt werden können, um die Qualität vergleichbar zu machen.
- Algorithmen sind aber von ihren Umgebungen (Betriebssysteme, Programmiersprachen), abstrahiert, daher kann es schwierig werden zu sagen wie lange ein Algorithmus benötigt
- die Laufzeit eines Algorithmus gibt es nicht

### Wer ist der Leichteste?

Aufgabe: Den Leichtesten finden
Problem: Den Leichtesten finden
Eingabe: eine reihe von n Männern
Berechne: die Nummer eines Mannes, der höchstens so viel wiegt wie jeder der anderen Männer in der Reihe

Methode 1: alle nacheinander prüfen. Benötigt 20 Durchläufe
Methode 2: der schwerste scheidet aus. Benötigt 4 Durchläufe

Siehe 5_find_the_lightest.rs

- In der *experimentellen Algorithmik* würden beide Algorithmen gegeneinander laufen und die Laufzeit gemessen. Mehrere Durchläufe wären wegen technischer Gegebenheiten nötig. Auch die verfügbare Menge von Speicher oder Implementierung, könnte Einfluss auf das Ergebnis haben, so dass nicht sicher ist, welcher der Algorithmus (nicht die Methode im Computer) der bessere ist.
- Eine weitere Methode wäre eine abstrakte Modellmaschine mit festen Zeiteinheiten wie die RAM zu nutzen. Jeder Speicheraufruf, Schritt, Durchlauf des Algorithmus wird einer Zeiteinheit festgelegt und mit den durchlaufenden Schritten multipliziert. Dies ergibt die Rechenzeit für den Algorithmus (nicht für das Computerprogramm oder die Methode in einer Programmiersprache).
- Die Analyse von einfachen Laufzeitmodellen ist jedoch oft zu mühselig und lohnt bei komplexen Algorithmen oft nicht, da sie eben nur bedingt aussagekräftig in der „realen Welt“, auf dem Computer.
- Aus den beiden genannten Gründen, hat sich die *asymptotische Laufzeitanalyse* für den Laufzeitvergleich von Algorithmen durchgesetzt.

### Asymptotische Laufzeitanalyse

- Betrachtet auch die Anzahl der Eingaben. Auch Eingabemenge genannt (z.B. die Menge der Männer deren Gewicht verglichen werden muss).
- Die asymptotische Laufzeitanalyse betrachtet wie sich die Laufzeit mit der größe der Eingabelänge verändert.
- Im Beispiel „Finde den Leichtesten“ Methode 2, verdoppelt sich die Anzahl der Schleifendurchläufe, wenn man die Anzahl der zu vergleichenden Männer verdoppelt. Es besteht ein linearer Zusammenhang zwischen Eingabelänge und Laufzeit.
- Man konzentriert sich ausschließlich auf die generelle Entwicklung der Laufzeit und lässt die realen Bedingungen realer Computer außer acht.
- Die asymptotische Laufzeitanalyse ist die theoretische Betrachtung der Laufzeit und „das Mittel der Wahl“

### Laufzeitanalysen

- Mathematiker verwenden den Begriff „abschätzen“, um die schlechteste Laufzeit eines Algorithmus mit Hilfe von Ungleichungen nach oben einzugrenzen.
- Die worst-case Laufzeit *T(n)* „nach oben hin abzugrenzen“ bedeutet, einen Wert *g* (Grenze nach oben) zu finden, für den die Ungleichung *T(n) <= g* gilt.





# Glossar

- **asymptotische Laufzeitanalyse**
- **Big-0-Analysis (ext)**
- **Induktionsbeweis**
- **Korrektheitsbeweis**
- **RAM Random Access Machine**, Maschine mit wahlfreiem Speicherzugriff, nicht zu verwechseln mit Random Access Memory

# Video-Quellen#

- Lec 1 – MIT Algorithms https://www.youtube.com/watch?v=JPyuH4qXLZ0 (ext)